from itertools import groupby


def chec(stroka):
    for c, v in groupby(stroka):
        count = len(list(v))
        return f'{c}{count if count > 1 else ""}'
