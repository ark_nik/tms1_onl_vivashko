from unittest import TestCase
from unittest import expectedFailure
from main_task_18_2 import game


class TestGame(TestCase):

    def test_game_1(self):
        self.assertEqual(game("1234", "1234"), "Вы выйграли")

    def test_game_2(self):
        self.assertNotEqual(game("1234", "9376"), "Быки: 1, Коровы: 3")

    def test_game_3(self):
        self.assertEqual(game("1234", "4256"), "Быки: 1, Коровы: 1")

    @expectedFailure
    def test_game_4(self):
        self.assertEqual(game("1234", "2345"), "Вы выйграли")

    @expectedFailure
    def test_game_5(self):
        self.assertNotEqual(game("1234", "1234"), "Вы выйграли")
