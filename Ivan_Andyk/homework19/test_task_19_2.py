import pytest
from main_task_19_2 import game


@pytest.fixture
def first_test():
    print("Check before starting")


class TestString:

    @pytest.mark.smoke
    @pytest.mark.parametrize("pc_number, answer, result",
                             [("1234", "1234", "Вы выйграли"),
                              ("1234", "9376", "Быки: 0, Коровы: 1"),
                              ("1234", "4256", "Быки: 1, Коровы: 1")])
    def test_success(self, pc_number, answer, result, first_test):
        assert game(pc_number, answer) == result

    @pytest.mark.xfail(reason="not in scope")
    @pytest.mark.parametrize("pc_number, answer",
                             [("111", "2345"),
                              ("sdfsf", "1234"),
                              ("1234", "sdfs")])
    def test_game_3(self, pc_number, answer, message):
        assert game(pc_number, answer) == message
