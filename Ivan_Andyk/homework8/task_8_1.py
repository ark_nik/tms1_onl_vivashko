def typed(my_type):
    def decorator(func):
        def wrapper(*args):
            if my_type == 'str':
                args = map(str, args)
            elif my_type == 'int':
                args = map(int, args)
            elif my_type == 'float':
                args = map(float, args)
            return func(*args)
        return wrapper
    return decorator


@typed(my_type='str')
def add_two_symbols(a, b):
    return a + b


print(add_two_symbols("3", 5))
print(add_two_symbols(5, 5))
print(add_two_symbols('a', 'b'))


@typed(my_type='int')
def add_three_symbols(a, b, c):
    return a + b + c


print(add_three_symbols(5, 6, 7))
print(add_three_symbols("3", 5, 0))


@typed(my_type='float')
def add_three_symbols(a, b, c):
    return a + b + c


print(add_three_symbols(0.1, 0.2, 0.4))
