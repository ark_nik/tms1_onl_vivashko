"""Работа с yaml файлом."""

import yaml
import json

with open("order.yaml") as f:
    data = yaml.safe_load(f)

# Напечатать номер заказа, адрес отправки
print("Номер заказа:", data["invoice"])
print("Адрес отправки:", data["bill-to"]["address"])

# Напечатать описание посылки, стоимость, кол-во
for i in data["product"]:
    desc, pr, qua = i["description"], i["price"], i["quantity"]
    print(f"Описание: {desc}, цена: {pr}, кол-во: {qua}")

# Сконвертировать yaml файл в json
with open("yaml_json.json", "w") as j_file:
    json.dump(str(data), j_file)

# Создать свой yaml файл
yaml_create = {"User1": {"First Name": "Kseniya",
                         "Last Name": "Likhtarovich",
                         "Login": "admin",
                         "Password": "admin"}}

with open("yaml_file.yaml", "w") as file:
    yaml.dump(yaml_create, file)
