from selenium import webdriver
import pytest


@pytest.fixture
def browser():
    driver = webdriver.Chrome()
    driver.implicitly_wait(5)
    driver.get("http://the-internet.herokuapp.com/dynamic_controls")
    yield driver
    driver.quit()
