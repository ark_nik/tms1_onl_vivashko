from Kseniya_Likhtarovich.hm_25.pages.base_page import BasePage
from Kseniya_Likhtarovich.hm_25.locators.main_page_locators \
    import MainPageLocators


class MainPage(BasePage):

    def open_login_page(self):
        sign_in_button = self.find_element(MainPageLocators.LOCATOR_SIGN_IN)
        sign_in_button.click()

    def open_cart_page(self):
        cart_link = self.find_element(MainPageLocators.LOCATOR_CART_LINK)
        cart_link.click()
