class String:

    def count_letters(self, s: str):
        stroka_new = s[0]
        count = 0
        for i in range(len(s) - 1):
            if s[i] == s[i + 1]:
                count += 1
                if i == len(s) - 2:
                    stroka_new += str(count + 1)
            else:
                if count > 0:
                    stroka_new += str(count + 1)
                    count = 0
                stroka_new += s[i + 1]
        return stroka_new
