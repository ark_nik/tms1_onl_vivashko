import pytest
from selenium import webdriver


@pytest.fixture
def browser():
    driver = webdriver.Chrome("./chromedriver 2")
    yield driver
    driver.quit()
