from selenium.webdriver.common.by import By


class MainPageLocators:

    LOCATOR_SIGN_IN_BUTTON = (By.XPATH, "//a[@class='login']")
    LOCATOR_BEST_SELLERS_BUTTON = (By.XPATH, "//a[text()='Best Sellers']")
    LOCATOR_GO_TO_CART_PAGE = (By.XPATH, "//a[@title='View my shopping cart']")
    LOCATOR_WOMEN_TAB = (By.XPATH, "//a[@title='Women']")
    LOCATOR_DRESSES_TAB = (By.XPATH, "//*[@id='block_top_menu']/ul/li[2]/a")
    LOCATOR_TSHIRTS_TAB = (By.XPATH, "//*[@id='block_top_menu']/ul/li[3]/a")
    LOCATOR_WOMEN_TAB_TEXT = (By.XPATH, "//span[text()='Women']")
    LOCATOR_DRESSES_TAB_TEXT = (By.XPATH, "//span[@class='category-name']")
    LOCATOR_TSHIRTS_TAB_TEXT = (By.XPATH, "//span[@class='category-name']")

    LOCATOR_PRINTED_DRESS = (By.XPATH, "//img[@title='Printed Dress']")
    LOCATOR_ADD_TO_CART =\
        (By.XPATH, "//*[@id='homefeatured']/li[3]/div/div[2]/div[2]/a[1]/span")
    LOCATOR_CONTINUE_SHOPPING = \
        (By.XPATH,
         "//span[@class='continue btn btn-default button exclusive-medium']")
