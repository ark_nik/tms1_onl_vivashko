from pages.main_page import MainPage
from locators.Main_Page_Locators import MainPageLocators


def test_switch_between_tabs(browser):
    main_page = MainPage(browser)
    main_page.open_main_page()
    main_page.should_be_main_page()
    main_page.switch_between_tabs(MainPageLocators.LOCATOR_WOMEN_TAB)
    main_page.should_be_women_tab()
    main_page.switch_between_tabs(MainPageLocators.LOCATOR_DRESSES_TAB)
    main_page.should_be_dresses_tab()
    main_page.switch_between_tabs(MainPageLocators.LOCATOR_TSHIRTS_TAB)
    main_page.should_be_tshirts_tab()
