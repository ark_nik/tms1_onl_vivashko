def typed(need_type):
    def decorator(func):
        def wrapper(*args):
            if need_type == "str":
                args = map(str, args)
            elif need_type == "int":
                args = map(int, args)
            elif need_type == "float":
                args = map(float, args)
            print(func(*args))

        return wrapper

    return decorator


@typed(need_type='str')
def add(a, b):
    return a + b


add('3', 5)
add(5, 5)
add('a', 'b')


@typed(need_type='int')
def add(a, b, с):
    return a + b + с


add(5, 6, 7)
add("3", 5, "100")


@typed(need_type='float')
def add(a, b, с):
    return a + b + с


add(0.1, 0.2, 0.4)
