number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three',
                4: 'four', 5: 'five', 6: 'six', 7: 'seven',
                8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven',
                12: 'twelve', 13: 'thirteen', 14: 'fourteen',
                15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
                18: 'eighteen', 19: 'nineteen'}


def my_decorator(func):
    def wrapper(str_input: str):
        string_word = []
        string_number_sorted = []
        for k, v in number_names.items():
            for i in str_input:
                if i == str(k):
                    string_word.append(v)
        string_word_sorted = sorted(string_word)
        print(string_word_sorted)
        for i in string_word_sorted:
            for k, v in number_names.items():
                if i == v:
                    string_number_sorted.append(k)
        func(string_number_sorted)

    return wrapper


@my_decorator
def sorted_function(str_input):
    print(str_input)


str_input = input("Введи числа через пробел:")
sorted_function(str_input)
