from selenium.common.exceptions import NoSuchElementException


def test_dynamic(browser):
    browser.get('http://the-internet.herokuapp.com/dynamic_controls')
    browser.find_element_by_xpath(
        '//*[@id="checkbox"]/input').click()
    browser.find_element_by_xpath(
        '//*[@id="checkbox-example"]/button').click()
    assert browser.find_element_by_xpath(
        '//*[@id="message"]').text == "It's gone!"
    try:
        browser.find_element_by_xpath('//*[@id="checkbox"]/input')
    except NoSuchElementException:
        return False
    return True


def test_input(browser):
    browser.get('http://the-internet.herokuapp.com/dynamic_controls')
    input_element = browser.find_element_by_xpath(
        '//*[@id="input-example"]/input')
    assert input_element.is_enabled() is False
    browser.find_element_by_xpath(
        '//*[@id="input-example"]/button').click()
    assert browser.find_element_by_xpath(
        '//*[@id="message"]').text == "It's enabled!"
    assert input_element.is_enabled()
