from collections import Counter
import string


def str_text(input_text):
    text = ""
    for condition in input_text:
        if condition != "" and condition not in string.punctuation \
                and condition.isdigit() is False:
            text += condition.lower()
    count_text = Counter(text)
    final_text = []
    for k in count_text.keys():
        if count_text[k] == max(count_text.values()):
            final_text.append(k)
    final_text = sorted(final_text)
    print(f"Самая частая буква в тексте {input_text}: {final_text[0]}")


str_text("a-z")
str_text("Hello World!")
str_text("How do you do?")
str_text("One")
str_text("Oops!")
str_text("AAaooo!!!!")
str_text("a" * 9000 + "b" * 1000)
str_text("199GGwp333ggWP!!!!")
