def typed(type):
    def decorator(func):
        def wrapper(*args):
            new_argument = []
            for i in args:
                if type == "str":
                    new_argument.append(str(i))
                elif type == "int":
                    new_argument.append(int(i))
                elif type == "float":
                    new_argument.append(float(i))
            return func(*new_argument)
        return wrapper
    return decorator


@typed(type="str")
def add_two_symbols(a, b):
    return a + b


print(add_two_symbols("3", 5))
print(add_two_symbols(5, 5))
print(add_two_symbols('a', 'b'))


@typed(type="int")
def add_three_symbols(a, b, c):
    return a + b + c


print(add_three_symbols(5, 6, 7))
print(add_three_symbols("3", 5, 0))
print(add_three_symbols(0.1, 0.2, 0.4))
