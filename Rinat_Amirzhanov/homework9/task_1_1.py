class Library:
    def __init__(self, book_name, author, pages, isbn, flag):
        self.book_name = book_name
        self.author = author
        self.pages = pages
        self.ISBN = isbn
        self.flag = flag
        self.reserved = True

    def book_status(self):
        if self.reserved:
            print(f"Книга {self.book_name} доступна\n")
            return True
        else:
            print(f"Книга {self.book_name} на данный момент занята")
            return False


class User:
    def __init__(self, username):
        self.name = username

    def get_book(self, book):
        if book.book_status():
            print(f"Книга {book.book_name} получена")
            book.reserved = False

    def reserve_book(self, book):
        if book.book_status():
            book.reserved = False

    def return_book(self, book):
        book.reserved = True
        print(f"Спасибо {self.name} что вернули книгу")


Abay = Library(
    book_name="Путь Абая",
    author="М. Ауэзов",
    pages=110,
    isbn="978-601-294-108-1",
    flag="KZ")

Fahrenheit451 = Library(
    book_name="451 градус по Фаренгейту",
    author="R. Bradbury",
    pages=192,
    isbn="978-5-9908664-9-2",
    flag="USA")

Dorian_portrait = Library(
    book_name="Портрет Дориана Грея",
    author="Уайльд О., Томас Р., Фиумара С.",
    pages=144,
    isbn="978-5-17-114855-3",
    flag="PL"
)

people_1 = User(username="Rinat")
people_2 = User(username="Ruslan")

people_1.get_book(book=Abay)
people_2.get_book(book=Abay)

people_2.get_book(book=Fahrenheit451)
people_1.return_book(book=Abay)
