class Invest:
    def __init__(self, inv_sum, inv_month, percent):
        self.inv_sum = inv_sum
        self.inv_month = inv_month
        self.percent = percent


class Bank:
    def deposit(self, invest):
        for i in range(1, invest.inv_month + 1):
            invest.inv_sum += \
                (invest.inv_sum * (invest.percent / 100) * 30) / 360
            print(f"{i} месяц:", invest.inv_sum)
        return f"Итоговая сумма на счету: {invest.inv_sum}"


# Формулу взял из https://kaspi.kz/guide/ru/deposit/#q578
deposit = Invest(500000, 12, 8.65)
bank = Bank()
print(bank.deposit(deposit))
