import unittest
from unittest import expectedFailure
from main_bulls_cows import bulls_cows


class Test_Bulls_Cows(unittest.TestCase):

    def test_bulls_3(self):
        actual = bulls_cows(4569, 4568)
        expected = [3, 0]
        self.assertEqual(actual, expected)

    def test_bulls_2(self):
        actual = bulls_cows(4569, 4500)
        expected = [2, 0]
        self.assertEqual(actual, expected)

    def test_bulls_1(self):
        actual = bulls_cows(4569, 4000)
        expected = [1, 0]
        self.assertEqual(actual, expected)

    def test_cows_4(self):
        actual = bulls_cows(4569, 9654)
        expected = [0, 4]
        self.assertEqual(actual, expected)

    def test_cows_3(self):
        actual = bulls_cows(4569, 9054)
        expected = [0, 3]
        self.assertEqual(actual, expected)

    def test_cows_2(self):
        actual = bulls_cows(4569, 9004)
        expected = [0, 2]
        self.assertEqual(actual, expected)

    def test_cows_1(self):
        actual = bulls_cows(4569, 3004)
        expected = [0, 1]
        self.assertEqual(actual, expected)

    def test_total_failure(self):
        actual = bulls_cows(4569, 1111)
        expected = [0, 0]
        self.assertEqual(actual, expected)

    def test_win(self):
        actual = bulls_cows(4569, 4569)
        expected = [4, 0]
        self.assertEqual(actual, expected)

    @expectedFailure
    def test_less_then_four_symbols(self):
        actual = bulls_cows(4569, 111)
        self.fail(actual)

    @expectedFailure
    def test_more_then_four_symbols(self):
        actual = bulls_cows(4569, 11111)
        self.fail(actual)

    @expectedFailure
    def test_with_not_int(self):
        actual = bulls_cows(4569, "abc")
        self.fail(actual)

    @expectedFailure
    def test_empty(self):
        actual = bulls_cows(4569, "")
        self.fail(actual)
