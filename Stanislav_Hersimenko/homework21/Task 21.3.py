import json

with open("students.json", "r") as read_file:
    data = json.load(read_file)


def find_by_one_class_section(data, Class, Section):
    def condition(item):
        return item['Class'].lower(
        ) == str(Class).lower() and item['Club'].lower(
        ) == str(Section).lower()

    filtered = [d for d in data if condition(d)]

    print(f'Finding all class students {Class} in {Section} club')
    for item in filtered:
        print(item)


def sort_gender(data):
    gender = sorted(data, key=lambda x: x['Gender'])

    for item in gender:
        print(item)


def find_by_name(Name):
    res = [i for i in data if str(Name.lower()) in i['Name'].lower()]

    print(f'Finding all Names with substring {Name}')
    for item in res:
        print(item)


sort_gender(data)
find_by_one_class_section(data, '5a', 'Football')
find_by_name('sota')
