import yaml
import json

with open('order.yaml') as f:
    goods = yaml.safe_load(f)

print("Invoice:", goods["invoice"])
print("Destination:", goods["bill-to"]["address"])

for elem in goods['product']:
    quantity = elem['quantity']
    description = elem['description']
    price = elem['price']
    print(f'Quantity - {quantity},'
          f'Description - {description},'
          f'Price - {price}')

with open("new_json_file.json", "w") as json_file:
    json.dump(str(goods), json_file)

new_yaml_file = {"Random text": "something is writen"}

with open('new_order.yaml', 'w') as f:
    yaml.dump(new_yaml_file, f)
