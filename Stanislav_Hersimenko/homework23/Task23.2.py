from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep


def test_login():
    url = "https://ultimateqa.com/filling-out-forms/ "
    path = "C:/chromedriver.exe"
    driver = webdriver.Chrome(path)
    driver.get(url)
    Name = "Test_User"
    Massage = "Random Text"
    Name_input = driver.find_element(By.ID, "et_pb_contact_name_0")
    Name_input.send_keys(Name)
    Massage_input = driver.find_element(By.ID, "et_pb_contact_message_0")
    Massage_input.send_keys(Massage)
    button = driver.find_element(
        By.XPATH, '//*[@id="et_pb_contact_form_0"]//button')
    button.click()
    sleep(5)
    contact_message = driver.find_element(
        By.XPATH, '//*[@id="et_pb_contact_form_0"]//p').text
    assert "Please refresh the page and try again." == contact_message
