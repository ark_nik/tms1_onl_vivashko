from time import sleep
from selenium import webdriver


def test_wiki_search_word():
    url = 'https://en.wikipedia.org/'
    word = 'Python (programming language)'
    driver = webdriver.Chrome('./chromedriver.exe')
    driver.get(url)
    search_field = driver.find_element_by_id('searchInput')
    search_field.send_keys(word)
    search_field.submit()
    sleep(2)
    article_title = driver.find_element_by_id('firstHeading').text
    assert word == article_title, f'{article_title} is not equal {word}'
    driver.quit()
