a = "this is a test string"
b = "ymnx nx f yjxy xywnsl"
z = 5


def encode(str1: str, n):
    eng = 'abcdefghijklmnopqrstuvwxyz'
    for x in str1:
        if x in eng:
            x = eng[eng.index(x) + n]
        yield x


def decode(str1: str, n):
    eng = 'abcdefghijklmnopqrstuvwxyz'
    for x in str1:
        if x in eng:
            x = eng[eng.index(x) - n]
        yield x


print("Encode result:")
for i in encode(a, z):
    print(i, end='')
print()
print("Decode result:")
for i in decode(b, z):
    print(i, end='')
