import pytest
from strings_w_symbol import count_letters


@pytest.fixture(params=["a4b2c"])
def preparation(request):
    return request.param


@pytest.mark.smoke
class TestSmoke:
    def test_positive(self, preparation):
        assert count_letters("aaaabbc") == preparation


@pytest.mark.xfail(reason="numbers used instead of letters")
class TestNumbers:
    def test_negative_numbers_in_phrase(self):
        assert count_letters("aaa33dd") == "a3d2"


@pytest.mark.skip(reason="we won't implement this")
class TestSpaces:
    def test_negative_space_in_phrase(self):
        assert count_letters("   ") == " 3"
