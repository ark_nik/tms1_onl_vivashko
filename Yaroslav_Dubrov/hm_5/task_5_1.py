import random

# Generating PC number with unique numbers
j = 10
pc = []
while j > 0:
    r = random.randint(1, 9)
    if r not in pc:
        pc.append(r)
    if len(pc) >= 4:
        break
    j -= 1

print(f"Only for debugging purposes! PC number is: {pc}")

# Game part
user = []

while pc != user:
    u_input = input("Please enter your 4 digits number: ")

# Convert user number to list
    user = list(map(int, str(u_input)))

# Finding cows as pairs of duplicate values in pc and user lists
    res = [x for x in pc + user if x in pc and x in user]

# Count of pairs in res list = count of cows
    if len(res) == 2:
        print("Cows count: 1")
    elif len(res) == 4:
        print("Cows count: 2")
    elif len(res) == 6:
        print("Cows count: 3")
    elif len(res) == 8:
        print("Cows count: 4")

# Finding bulls compiling list with matched by index values
    bulls = []
    for i in range(len(pc)):
        if pc[i] == user[i]:
            bulls.append(pc[i])
    print(f"Bulls count: {len(bulls)}")
else:
    print("You win!")
