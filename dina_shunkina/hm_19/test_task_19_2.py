from dina_shunkina.hm_19.main_2 import game
import pytest


@pytest.fixture(scope="class")
def start_work():
    print("Let's check our tests!")


class TestCasesForPositive:
    @pytest.fixture
    def input_number(self, start_work):
        return "3219"

    @pytest.mark.smoke
    @pytest.mark.positive
    def test_positive_1(self, start_work, input_number):
        print(f"{input_number}")
        assert game(input_number) == (1, 2), "There is not a mistake"

    @pytest.mark.positive
    @pytest.mark.parametrize("number", ["1234"])
    def test_positive_2(self, number):
        assert game(number) == (4, 0), "There is not a mistake"


class TestCasesForNegative:

    @pytest.mark.smoke
    @pytest.mark.negative
    @pytest.mark.xfail(reason="Empty values")
    def test_negative_2(self, start_work):
        assert game("")

    @pytest.mark.negative
    @pytest.mark.xfail(reason="Special characters")
    def test_negative_1(self, start_work):
        assert game("1#3#4")
