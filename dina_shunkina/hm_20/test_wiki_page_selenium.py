from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
import pytest


@pytest.fixture(scope="session")
def browser():
    driver = webdriver.Chrome("./chromedriver")
    driver.maximize_window()
    sleep(1)
    yield driver
    driver.quit()


def test_find_word(browser):
    url = "https://www.wikipedia.org/"
    word = "Programming language"
    browser.get(url)
    search_filed = browser.find_element(By.ID, "searchInput")
    search_filed.send_keys(word)
    sleep(1)
    search_filed.submit()
    sleep(1)
    name_title = browser.find_element(By.ID, "firstHeading").text
    sleep(1)
    assert name_title == word, f"{name_title} is not {word}"
