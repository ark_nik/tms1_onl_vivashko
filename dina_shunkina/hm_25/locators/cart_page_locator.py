from selenium.webdriver.common.by import By


class CartPageLocator:
    LOCATOR_YOUR_SHOPPING_CART_TEXT = (By.CSS_SELECTOR, ".navigation_page")
    LOCATOR_CART_IS_EMPY_TEXT = (
        By.XPATH, "//p[text()='Your shopping cart is empty.']")
    LOCATOR_HOME_ICOM = (By.CSS_SELECTOR, ".icon-home")
