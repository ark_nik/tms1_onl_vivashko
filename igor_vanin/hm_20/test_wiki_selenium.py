from selenium import webdriver
import pytest
text_search = "Python (programming language)"
title_result = "Python (programming language)"
input_search = '//*[@id="searchInput"]'
search_button = '//*[@id="searchButton"]'
title = '//h1[@class="firstHeading"]'


@pytest.fixture
def browser():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()


def test_selenium_wiki(browser):
    browser.get("https://en.wikipedia.org/")
    browser.find_element_by_xpath(input_search).send_keys(text_search)
    browser.find_element_by_xpath(search_button).click()
    title_wiki = browser.find_element_by_xpath(title).text
    assert title_wiki == title_result
