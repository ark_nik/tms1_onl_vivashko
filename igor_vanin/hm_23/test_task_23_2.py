from selenium import webdriver
from selenium.webdriver.common.by import By


def test_lesson_24_1():
    url = 'https://ultimateqa.com/filling-out-forms/'
    result = 'Filling Out Forms - Ultimate QA'
    result_name = 'Name'
    result_message = "Message"
    loc_name = '[id="et_pb_contact_name_0"]'
    loc_message = '[name="et_pb_contact_message_0"]'
    loc_button = '.et_pb_contact_form_0 > :nth-child(2)' \
                 ' .et_pb_contact_submit.et_pb_button'
    loc_error_name = '#et_pb_contact_form_0 > .et-pb-contact-message li'

    browser = webdriver.Chrome()
    browser.implicitly_wait(7)
    browser.get(url)
    browser.find_element(By.CSS_SELECTOR, loc_name).send_keys("Name")
    browser.find_element(By.CSS_SELECTOR, loc_message).send_keys("Comment")
    browser.find_element(By.CSS_SELECTOR, loc_button).click()
    title = browser.title
    assert result == title
    browser.refresh()
    browser.find_element(By.CSS_SELECTOR, loc_name).send_keys("Name")
    browser.find_element(By.CSS_SELECTOR, loc_button).click()
    error = browser.find_element(By.CSS_SELECTOR, loc_error_name).text
    assert error == result_message
    browser.refresh()
    browser.find_element(By.CSS_SELECTOR, loc_message).send_keys("message")
    browser.find_element(By.CSS_SELECTOR, loc_button).click()
    error = browser.find_element(By.CSS_SELECTOR, loc_error_name).text
    assert error == result_name
