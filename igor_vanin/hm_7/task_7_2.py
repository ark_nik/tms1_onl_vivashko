# 1 Генераторы:
numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
gen_numbers_one = [i for i in numbers if i > 0]
print(gen_numbers_one)

# 2 Генераторы yield:


def generator_numbers(numbers_list: list):
    for i in numbers_list:
        if i > 0:
            yield i


print([i for i in generator_numbers(numbers)])
