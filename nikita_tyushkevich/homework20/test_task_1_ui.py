from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By


def test_wiki():
    link = 'https://en.wikipedia.org/'
    word = 'Python (programming language)'
    print("\nstart browser for test..")
    driver = webdriver.Chrome('./chromedriver.exe')
    driver.get(link)
    search_field = driver.find_element(By.ID, 'searchInput')
    search_field.send_keys(word)
    search_field.submit()
    sleep(3)
    article_title = driver.find_element(By.ID, 'firstHeading').text
    assert word == article_title, f"{article_title} is not {word}"
    driver.quit()
