import xml.etree.ElementTree as ET


def parse_xml(param, value):
    """Find book by parameter"""
    for child in root:
        # print(child.attrib)
        for appt in child:
            if appt.tag == param and str(value) in str(appt.text):
                return parse_info(child.attrib["id"])


def parse_info(id):
    """Display product info"""
    for child in root:
        if id == child.attrib["id"]:
            for i in child:
                print(i.tag + " - " + i.text)


root = ET.parse("library.xml").getroot()
parse_xml("price", 7)
