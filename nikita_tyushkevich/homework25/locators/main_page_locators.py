from selenium.webdriver.common.by import By


class MainPageLocators:

    LOCATOR_SIGN_IN = (By.CLASS_NAME, 'login')
    LOCATOR_BREADCRUMB = (By.CLASS_NAME, 'breadcrumb clearfix')
    LOCATOR_CART = (By.XPATH, '//div[@class="shopping_cart"]/a')
