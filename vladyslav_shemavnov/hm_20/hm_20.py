import wikipediaapi
from selenium import webdriver


def test_with_ui():
    browser = webdriver.Chrome()
    browser.get("https://en.wikipedia.org/")

    search_field = browser.find_element_by_id("searchInput")
    search_field.send_keys("Python (programming_language)")
    search_button = browser.find_element_by_id("searchButton")
    search_button.click()

    search_result = browser.find_element_by_id("firstHeading")
    print(search_result.text)

    assert search_result.text == "Python (programming language)", \
        "Search result does not match"


def test_with_api():
    wiki_wiki = wikipediaapi.Wikipedia('en')
    wiki_page = 'Python_(programming_language)'
    page_py = wiki_wiki.page(wiki_page)
    print(page_py.title)
    assert page_py.title == wiki_page, "Api different"
