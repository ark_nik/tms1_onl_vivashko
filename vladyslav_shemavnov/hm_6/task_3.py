def division(a, b):
    result = None
    if b == 0:
        print("На ноль делить нельзя")
        pass
    else:
        result = a / b
    return result


def addition(a, b):
    return a + b


def subtraction(a, b):
    return a - b


def multiplication(a, b):
    return a * b


def calc():
    action = input("Введите действие "
                   "калькулятора *, /, +, - :")
    a = int(input("Введите первое число: "))
    b = int(input("Введите второе число: "))

    action_dict = {
        "*": multiplication(a, b),
        "/": division(a, b),
        "+": addition(a, b),
        "-": subtraction(a, b)
    }

    if action in action_dict:
        print(f"Ваш результат: {action_dict[action]}")


calc()
